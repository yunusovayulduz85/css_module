import React from 'react'
import { Error, Wrapper } from '../styles'

const notFound = () => {
  return (
    <Wrapper>

      <Error>404 Not Found</Error>
    </Wrapper>
  )
}

export default notFound