import React from 'react'
import { Button, Content, Input, Registration, Text, Title, Wrapper, WrapperButton, WrapperForm, WrapperInputLabel } from '../styles'
const Login = () => {
  return (
    <Wrapper>
      <Content>
        <Registration>
          <WrapperForm>
            <Title>Login</Title>
            <WrapperInputLabel>
              <Text>Full Name</Text>
              <Input></Input> <br />
            </WrapperInputLabel>
            <WrapperInputLabel>
              <Text>Email</Text>
              <Input></Input> <br />
            </WrapperInputLabel>
          </WrapperForm>
          <WrapperButton>
            <Button>Sign In</Button>
          </WrapperButton>
        </Registration>
      </Content>
    </Wrapper>
  )
}

export default Login