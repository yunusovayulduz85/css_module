import React from 'react'
import { Button, Content, Input, Registration, Text, Title, Wrapper, WrapperButton, WrapperForm, WrapperInputLabel } from '../styles'
import { Outlet } from 'react-router-dom'
const Register = () => {
    return (
        <Wrapper>
            <Content>
                <Registration>
                    <WrapperForm>
                        <Title>Registration</Title>
                        <WrapperInputLabel>
                            <Text>Full Name</Text>
                            <Input></Input> <br />
                        </WrapperInputLabel>
                        <WrapperInputLabel>
                            <Text>Email</Text>
                            <Input></Input> <br />
                        </WrapperInputLabel>
                        <WrapperInputLabel>
                            <Text>Phone Number</Text>
                            <Input></Input> <br />
                        </WrapperInputLabel>
                    <WrapperButton>
                        <Button>Sign Up</Button>
                    </WrapperButton>
                    </WrapperForm>
                </Registration>
            </Content>
            {/* <Outlet/> */}
        </Wrapper>
    )
}

export default Register