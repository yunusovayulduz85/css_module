import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Register from '../register/Register'
import Login from '../login/Login'
import NotFound from '../notFound/notFound'

const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/'>
          <Route index element={<Register />} />
          <Route path='/login' element={<Login />} />
          <Route path='*' element={<NotFound />} />
        </Route>
      </Routes>
    </BrowserRouter>
  )
}

export default Router