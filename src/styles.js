import styled from "styled-components";
export const Wrapper=styled.div`
    width: 100vw;
    max-width: 100vw;
    min-height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
`
export const Content=styled.div`
    padding: 10px 5px;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 500px;
    height: 500px;
    background-color: rgb(246, 246, 246);
    border-radius: 8px;
`
export const Registration=styled.form`
    width: 400px;
    height: 400px;
    background: rgb(69, 69, 69);
    border-radius: 8px;
    padding: 15px;
`
export const WrapperForm=styled.div`
    padding: 5px 30px;
`
export const Title=styled.h2`
    color: white;
    font-family: sans-serif;
    font-weight: bold;
    text-align: center;
`
export const WrapperInputLabel=styled.div`
    display: flex;
    flex-direction: column;
    gap: 10px;
`
export const Text=styled.label`
    color: white;
    font-size: 18px;
    font-family: sans-serif;
    font-weight: semibold;
`
export const Input=styled.input`
    padding:4px 3px;
    border-radius: 5px;
    outline: none;
    border: 2px solid transparent;
    width: 100%;

`
export const WrapperButton=styled.div`
    width: 100%;
    display: flex;
    justify-content: center;
    margin-top: -25px;
    /* padding-bottom: 15px; */

`
export const Button=styled.button`
    background: rgb(149, 147, 147);
    color: white;
`
export const Error=styled.h1`
    color:rgb(69, 69, 69) ;
`